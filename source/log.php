<?php
  session_start();
  if(isset($_COOKIE["token"])) {
      $token = $_COOKIE["token"];
  } else {
    header("Location: ../index.html");
    exit;
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="../img/logo/uhm-logo.png" type="image/png" sizes="17x17">
  <title>UHM Smart Logger</title>
  <!-- Font Awesome -->
  <link href="../font/fontawesome/css/all.min.css" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="../css/bootstrap.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="../css/mdb.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="../css/style.min.css" rel="stylesheet">
  <!-- Your custom datetimepicker (optional) -->
  <link href="../css/jquery.datetimepicker.min.css" rel="stylesheet">
  <!-- Your custom dataTable (optional) -->
  <link href="../css/datatables.css" rel="stylesheet">

  <!-- chart.js.css -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.css" rel="stylesheet">


  <style>

  .map-container{
    overflow:hidden;
    padding-bottom:56.25%;
    position:relative;
    height:0;
  }
  .map-container iframe{
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
  }

  .tr_color {color: #F5F5F5;}

    /* new nav active */
    .nav-active{
    border-bottom:6px solid #000000;
    border-radius: 3px;
  }
  .nav-pills .nav-link.active,
  .nav-pills .show > .nav-link {
    border-bottom: 5px solid #007bff;
  }
  .nav-menu {
    color: #007bff;
  }

  .dropdown-menu {
    left: auto;
    right: 0;
    float: right;
  }

  .nav-pills .nav-link {
    border-radius: 0rem;
  }
  .bg-image {
    background-image: url(../login/images/BG_09.jpg);
  }
  .image-image-image{
    /* background-image: url(../login/images/522343-earthquake.jpg); */
  }

  .highcharts-figure, .highcharts-data-table table {
    min-width: 360px; 
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}





  </style>
</head>

<body class="grey lighten-3">

  <!--Main Navigation-->
  <header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
      <div class="container-fluid">

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="nav nav-pills nav-pills-primary mr-auto" role="tablist">
            <li class="nav-item" style = "padding-right: 12px;">
              <a class="nav-link" style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href="./dashboard.php?timezone=<?php echo $_GET['timezone']; ?>" role="tablist" aria-expanded="true">
                <i class="fas fa-tram mr-1"></i> <B>DEVICE MANAGER</B>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#link1" role="tablist" aria-expanded="false">
                <i class="fas fa-chart-bar mr-1"></i> <B>DASHBOARD</B>
              </a>
            </li> -->
            
            <li class="nav-item" style = "padding-right: 12px;">
              <a class="nav-link" style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href='./view_chart.php?device=["WS000001"]&timezone=<?php echo $_GET['timezone']; ?>' role="tablist" aria-expanded="true">
                <i class="fas fa-chart-line mr-1"></i> <B>REAL TIME</B>
              </a>
            </li>


            <li class="nav-item" style = "padding-right: 12px;">
              <a class="nav-link active" style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href='./log.php?device=["WS000001"]&timezone=<?php echo $_GET['timezone']; ?>' role="tablist" aria-expanded="true">
                <i class="fas fa-table mr-1"></i> <B>Log</B>
              </a>
            </li>

            <!-- <li class="nav-item" style = "padding-right: 12px;">
              <a class="nav-link " style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href="./rawdata.php?timezone=<?php echo $_GET['timezone']; ?>" role="tablist" aria-expanded="false">
                <i class="fas fa-table mr-1"></i><B>DAQ DATA</B>
              </a>
            </li> -->

          </ul>

          <!-- Right -->
          <ul class="nav nav-menu">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">
                <i class="fas fa-address-card mr-1"></i><B><?php echo $_COOKIE["email"] ?></B>
              </a>
              <div class="dropdown-menu">
                <center>
                  <B><?php echo $_COOKIE["email"] ?></B>
                  <img src = "../login/images/admin.png" width="100px;"/>
                  <a class="dropdown-item">
                  <button id = "c_time"class='btn btn-outline-info btn-sm' data-toggle="modal" data-target="#change_datetime_show">Change Time</button><br>
                    <button id = "logout"class='btn btn-outline-danger btn-sm'>Logout</button>
                  </a>
              </center>
              </div>
            </li>
          </ul>

        </div>

      </div>
    </nav>
    <!-- Navbar -->

    <!-- Sidebar -->
    <div class="sidebar-fixed position-fixed bg-image">

      <!-- <a class="logo-wrapper waves-effect"> -->
      <div class="mb-1" style="padding-top: 0px;">
        <center><img src="../img/logo/uhm-logo.png"  alt="" style="width: 70%; height: 70%"></center>
      </div>
      <!-- </a> -->

      <!-- <M>Menu Config</M> -->
      <!-- <div class="mb-2"></div> -->

      <table class="table table-hover">
        <tbody>

          <tr>
            <th>Project</th>
            <td>UHM Smart Logger</td>
          </tr>
          <tr>
            <th>Date</th>
            <td id="real_date"></td>
          </tr>
          <!-- <tr>
            <th>Status</th>
            <td id="side_status"></td>
          </tr> -->   
    </tbody>
  </table>
</div>
<!-- Sidebar -->

</header>
<!--Main Navigation-->

<!--Main layout-->
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">
    <div class="tab-content tab-space">
    <!-- Page 2 -->
    <div class="tab-pane" id="link2" aria-expanded="true">
      <!-- Graph RAW -->
    
    </div>

    <div class="col-md-12  fadeIn" style="padding-left: 0px;padding-right: 0px;">
      <div class="card ">
        <div class="container">
          <div class="col-12">
            <div class="row">
              <div class='col-12'>
                <center>
                  <p><div style = "color: black;font-size: 1.5em;font-weight: 700;">
                  <?php 
                    if ($_GET['timezone'] == "0"){
                      echo "Time Zone [ UTC ]";
                    }else if ($_GET['timezone'] == '7') {
                      echo "Time Zone [ GMT Thailand ]";
                    }
                  ?>
                </div></p>
              </center>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>




    <!-- CH 1 -->
    <div class="tab-pane active" id="link1" aria-expanded="true">
      <div class="col-md-12 mb-4 fadeIn" style = "padding:0px !important;padding-left:0px !important;">
      <div class="col-md-12 mb-4 fadeIn" style = "padding-right:0px !important;padding-left:0px !important;">
        <div class="row">
          <!-- <div class="col-md-12"></div> -->
          <div class="col-md-12  fadeIn" >
            <div class="card ">
              <div class="container-fluid">
                <div class="col-12">
                  <div class="row">

                    <!-- space -->
                    <div class='col-3'>
                    
                    </div>

                    <div class='col-2'>
                    
                    <p class="mt-2">SELECT DEVICE</p>
                      <div class="input-group mt-3 mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-microchip"></i><!-- <i class="fas fa-calendar-week"></i> --></span>
                        </div>
                        
                        <!-- <input type="text" class="form-control"  id="SELECT_DEVICE_ID" value=""> -->
                        <select id = 'SELECT_DEVICE_ID' class="browser-default custom-select">
                          <option value="0" selected>NOT FOUND DEVICE</option>
                        </select>


                      </div>

                    </div>
                    <div class="col-2">

                        
                        <p class="mt-2">START DATE</p>
                        <div class="input-group mt-3 mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-calendar-week"></i></span>
                          </div>
                          <input type="text" class="form-control" placeholder="Start Date" aria-label="Start_Date" id="start_date_row" value="">
                        </div>


                      
                    </div>

                    <div class="col-2">
                      <p class="mt-2">STOP DATE</p>
                      <div class="input-group mt-3 mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-calendar-week"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Stop Date" aria-label="Stop_Date" id="stop_date_row" value="">
                      </div>
                    </div>

                    <div class="col-3">
                      <div class="mt-4 mb-10" style = "margin-top:40px !important;">
                        <button type="button" class="btn blue-gradient btn-rounded waves-effect waves-light" id="view_device" style="padding: 10px 30px;">VIEW</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>





      <div class="card">
        <div href="#CH1" class="card-header" id="GCH1"><i class="fas fa-table  fa-2x mr-2"></i><B>Table Log</B></div>
          <div class="card-body" id="CH1">

            <table id="example" class="table table-hover table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>Time</th>
                      <th>Battery (V)</th>
                      <th> Flow (m<sup>3</sup>/hr) </th>
                      <th> Volume (m<sup>3</sup>) </th>
                      <th>Pressure Inlet (bar)</th>
                      <th>Pressure Outlet (bar)</th>
                  </tr>
              </thead>
              <tbody id="table_log_body"></tbody>
          </table>

        </div>
      </div>
    </div>



    </div>


    </div>

  </div>
</main>
<!--Main layout-->

<!--Footer-->
<footer class="page-footer text-center font-small primary-color-dark darken-2 mt-4" style = "visibility: visible !important;background-color: #cbefff !important;">
  <!--Copyright-->
  <div class="footer-copyright py-2">
    © 2019 Copyright:
    <a href="https://www.deaware.com/" target="_blank"> Deaware.com </a>
  </div>
  <!--/.Copyright-->

</footer>
<!-- change time zone -->
<div class="modal fade" id="change_datetime_show" tabindex="-1" role="dialog" aria-labelledby="change_datetime_show_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="change_datetime_show_ModalLabel">Change Time Zone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <br>
                <div class='row'>
                    <div class = 'col-12'>
                        SELECT TIME ZONE:
                        <select id="select_timezone" class="browser-default custom-select">
                            <option selected="" value="0">SELECT TIME ZONE</option>
                            <option value="1">UTC</option>
                            <option value="2">GMT (Thailand)</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button onclick = "get_timezone()"id = "modal_confirm_add_device_to_event" type="button" class="button-save">Save</button>
                <button id = "modal_close" type="button" class="button-close " data-dismiss="modal">Close</button>
            </div>
      </div>
    </div>
</div>
<!--/.Footer-->

<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.js"></script>
<!-- fontawesome -->
<script type="text/javascript" src="../font/fontawesome/js/all.min.js"></script>
<!-- datetimepicker -->
<script type="text/javascript" src="../js/jquery.datetimepicker.full.min.js"></script>
<!-- dataTables -->
<script type="text/javascript" src="../js/datatables.js"></script>
<!-- Moment -->
<script type="text/javascript" src="../js/moment.js"></script>
<!-- Google Maps -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWYFDxAUrh4Lti5jjGI_gMhSZz5XU5zuo&callback=initMap" async defer></script> -->
<!-- Highstock -->
<script type="text/javascript" src="../js/addons/Highstock.js"></script>
<script type="text/javascript" src="../js/modules/exporting.js"></script>
<script type="text/javascript" src="../js/modules/export-data.js"></script>

<!-- JS Function -->
<script type="text/javascript" src="../js/function/page1.js"></script>
<script type="text/javascript" src="../js/function/page2.js"></script>
<script type="text/javascript" src="../js/function/page3.js"></script>
<script type="text/javascript" src="../js/function/option.js"></script>
<script type="text/javascript" src="../js/underscore.js"></script>

<!-- chart.js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>

<!-- this is axios-master github lib  -->
<!-- <script src="./../axios/axios-master/dist/axios.min.js"></script> -->


<script type="text/javascript">

  function view_view_device(){
    $("#view_device").on('click',function(){
      if ($("#SELECT_DEVICE_ID").val() == 0){
        alert("Please select DEVICE_ID")
      }else{
        // alert(device_arr[$("#SELECT_DEVICE_ID").val() - 1].id)

        var device = getCookie("device_now");
        var device_obj = JSON.parse(device)
        var start = $("#start_date_row").val()
        var stop = $("#stop_date_row").val()
        var deferent = (Math.floor(new Date(start).getTime()/1000.0 - new Date(stop).getTime()/1000.0));
        console.log(deferent)
        if (deferent > 0){
          alert("คุณเลือกเวลาผิด ")
        }else {
          window.location.replace("../source/log.php?device=[\"" + device_obj[$("#SELECT_DEVICE_ID").val() - 1] + "\"]" + "&timezone=<?php echo $_GET['timezone'];?>" + "&start_date="+start+"&stop_date="+stop);
        }
      }
    })
  }

  // is cook
  function cook_is_cook(){
    var content_out_stream = "";
    var device_id_get_php = JSON.parse('<?php echo $_GET['device'];?>')
    content_out_stream += "<option selected value = '0' selected>SELECT DEVICE ID</option>"
    var val = JSON.parse(getCookie('device_now'))
    for (var i = 0;i< val.length;i++){
      if (val[i] != null){
        var select = ''
        if (device_id_get_php[0]  === val[i]){
          select  = 'selected'
        }
        content_out_stream += "<option "+ select + " value = '"+(i+1)+"'>"+val[i]+"</option>"
      }
    }
    // content_out_stream += "<option value = '' selected>SELECT DEVICE ID</option>"
    $("#SELECT_DEVICE_ID").html("");
    $('#SELECT_DEVICE_ID').append(content_out_stream); 
  }

  function get_logs(device, start_date, stop_date){
    var html_body_table = ""
    var settings = {
      "url": "http://35.229.137.7/uhm_main/device/sensorlog",
      "method": "POST",
      "timeout": 0,
      "headers": {
        "Content-Type": "application/json"
      },
      "data": JSON.stringify({
        "id": device, 
        "start_date": start_date,
        "stop_date": stop_date
      }),
    };
    console.log(settings)
    $.ajax(settings).done(function (response) {

      // fliter resp.
      var new_log_element = []
      var groupedByHour = _.groupBy(response.result, function(item) {
          var dateMoment = moment(item.Time);
          return dateMoment.hour();
      });
      console.log(groupedByHour)
      for (const [key, value] of Object.entries(groupedByHour)) {
        if (value.length > 0){
          new_log_element.push(value[0])
        }
      }

      if (new_log_element !== undefined && new_log_element.length > 0){
        new_log_element.forEach((item ,index) => {
          html_body_table += "<tr>" +
            "<td>"+moment(item.Time).format('L HH:mm:ss')+"</td>" +
            "<td>"+((item.Batt === undefined) ? "0": item.Batt)+"</td>" +
            "<td>"+item.Flow_Accumulate+"</td>" +
            "<td>"+item.Flow+"</td>" +
            "<td>"+item.Pressure_1+"</td>" +
            "<td>"+item.Pressure_2+"</td>" +
          "</tr>" 
        })
        
        $("#table_log_body").append(html_body_table)

        $('#example').DataTable();
      }
    });
  }

  $( document ).ready(function() {
    cook_is_cook()
    real_date();

    $('#start_date_row').datetimepicker({
      timepicker: false,
      format:'Y-m-d'
    });
    $('#stop_date_row').datetimepicker({
      timepicker: false,
      format:'Y-m-d'
    });
    <?PHP 
      if (isset($_GET["start_date"]) && isset($_GET["start_date"])){
        echo "var check_date = true;";
        echo "var start_date = '" . $_GET["start_date"] . "';";
        echo "var stop_date = '" . $_GET["stop_date"] . "';";
      }else {
        echo "var check_date = false;";
        echo "var start_date = '';";
        echo "var stop_date = '';";
      }
    ?>
    if (check_date){
      $('#start_date_row').val(start_date);
      $('#stop_date_row').val(stop_date);

      var device = getCookie("device_now");
      var device_obj = JSON.parse(device)
      var select_device = device_obj[$("#SELECT_DEVICE_ID").val() - 1]
      get_logs(select_device, start_date, stop_date)
    }else {
      $('#start_date_row').val(moment().add(-1, "day").format('YYYY-MM-DD'));
      $('#stop_date_row').val(moment().format('YYYY-MM-DD'));

      var device = getCookie("device_now");
      var device_obj = JSON.parse(device)
      var select_device = device_obj[$("#SELECT_DEVICE_ID").val() - 1]
      get_logs(select_device, moment().add(-1, "day").format('YYYY-MM-DD'), moment().format('YYYY-MM-DD'))
    }
    // &start_date=2021-03-18&stop_date=2021-03-26
    
    
  
    view_view_device()
  })

</script>
</body>
</html>
